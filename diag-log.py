# Copyright 2023 Affe Null
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in the COPYING.md file.
# If not, see <http://www.gnu.org/licenses/>.

import struct
from diag import Diag

diag = Diag()

DIAG_VERNO_F = 0

DIAG_MSG_F = 31

DIAG_EXT_MSG_F = 121

DIAG_EXT_MSG_CONFIG_F = 125
MSG_EXT_SUBCMD_SET_RT_MASK = 4
MSG_EXT_SUBCMD_SET_ALL_RT_MASKS = 5

DIAG_EXT_MSG_TERSE_F = 126

DIAG_QSR_EXT_MSG_TERSE_F = 146

DIAG_CMD_EXT_F = 152

DIAG_QSR4_EXT_MSG_TERSE_F = 153

def set_rt_mask(ssid_start, ssid_end, mask):
    data = struct.pack('<HHxx', ssid_start, ssid_end)
    data += struct.pack('<I', mask) * (ssid_end - ssid_start + 1)
    diag.send_cmd(DIAG_EXT_MSG_CONFIG_F, MSG_EXT_SUBCMD_SET_RT_MASK, data=data)

def set_all_rt_masks(mask):
    data = struct.pack('<xI', mask)
    diag.send_cmd(DIAG_EXT_MSG_CONFIG_F, MSG_EXT_SUBCMD_SET_ALL_RT_MASKS, data=data)

print(diag.send_cmd(DIAG_VERNO_F))

KNOWN_SUBSYSTEMS = (
    ('LEGACY', 0),
    ('AUDFMT', 1),
    ('AVS', 2),
    ('BOOT', 3),
    ('BT', 4),
    ('CM', 5),
    ('DIAG', 7),
    ('DSM', 8),
    ('FS', 9),
    ('HS', 10),
    ('MDSP', 11),
    ('QDSP', 12),
    ('REX', 13),
    ('RF', 14),
    ('SD', 15),
    ('SIO', 16),
    ('VS', 17),
    ('WMS', 18),
    ('GPS', 19),
    ('MMOC', 20),
    ('RUIM', 21),
    ('TMC', 22),
    ('FTM', 23),
    ('MMGPS', 24),
    ('SLEEP', 25),
    ('SAM', 26),
    ('SRM', 27),
    ('SFAT', 28),
    ('JOYST', 29),
    ('MFLO', 30),
    ('DTV', 31),
    ('TCXOMGR', 32),
    ('EFS', 33),
    ('IRDA', 34),
    ('AAM', 36),
    ('BM', 37),
    ('PE', 38),
    ('QIPCALL', 39),
    ('FLUTE', 40),
    ('CAMERA', 41),
    ('HSUSB', 42),
    ('FC', 43),
    ('USBHOST', 44),
    ('PROFILER', 45),
    ('MGP', 46),
    ('MGPME', 47),
    ('GPSOS', 48),
    ('MGPPE', 49),
    ('GPSSM', 50),
    ('IMS', 51),
    ('MBP_RF', 52),
    ('SNS', 53),
    ('WM', 54),
    ('LK', 55),
    ('PWRDB', 56),
    ('DCVS', 57),
    ('ANDROID_ADB', 58),
    ('VIDEO_ENCODER', 59),
    ('VENC_OMX', 60),
    ('KINETO_GAN', 62),
    ('ANDROID_QCRIL', 63),
    ('A2', 64),
    ('LINUX_DATA', 65),
    ('QCNE', 68),
    ('APPS_CAD_GENERAL', 69),
    ('SRD_GENERAL', 75),
    ('ACDB_GENERAL', 76),
    ('TLE_XTM', 82),
    ('TLE_TLM_MM', 84),
    ('WWAN_LOC', 85),
    ('GNSS_LOCMW', 86),
    ('QSET', 87),
    ('ADC', 89),
    ('MMODE_QMI', 90),
    ('MCFG', 91),
    ('SSM', 92),
    ('MPOWER', 93),
    ('RMTS', 94),
    ('ADIE', 95),
    ('FLASH_SCRUB', 97),
    ('STRIDE', 98),
    ('POLICYMAN', 99),
    ('TMS', 100),
    ('LWIP', 101),
    ('RFS', 102),
    ('RFS_ACCESS', 103),
    ('RLC', 104),
    ('MEMHEAP', 105),
    ('WCI2', 106),
    ('ONCRPC', 500),
    ('ONCRPC_MISC_MODEM', 501),
    ('ONCRPC_MISC_APPS', 502),
    ('ONCRPC_CM_MODEM', 503),
    ('ONCRPC_CM_APPS', 504),
    ('ONCRPC_DB', 505),
    ('ONCRPC_SND', 506),
    ('1X', 1000),
    ('1X_DCP', 1002),
    ('1X_DEC', 1003),
    ('1X_ENC', 1004),
    ('1X_GPSSRCH', 1005),
    ('1X_MUX', 1006),
    ('1X_SRCH', 1007),
    ('HDR_PROT', 2000),
    ('HDR_DATA', 2001),
    ('HDR_SRCH', 2002),
    ('HDR_DRIVERS', 2003),
    ('HDR_IS890', 2004),
    ('HDR_DEBUG', 2005),
    ('HDR_HIT', 2006),
    ('HDR_PCP', 2007),
    ('HDR_HEAPMEM', 2008),
    ('UMTS', 3000),
    ('WCDMA_L1', 3001),
    ('WCDMA_L2', 3002),
    ('WCDMA_MAC', 3003),
    ('WCDMA_RLC', 3004),
    ('WCDMA_RRC', 3005),
    ('NAS_CNM', 3006),
    ('NAS_MM', 3007),
    ('NAS_MN', 3008),
    ('NAS_RABM', 3009),
    ('NAS_REG', 3010),
    ('NAS_SM', 3011),
    ('NAS_TC', 3012),
    ('NAS_CB', 3013),
    ('WCDMA_LEVEL', 3014),
    ('GSM', 4000),
    ('GSM_L1', 4001),
    ('GSM_L2', 4002),
    ('GSM_RR', 4003),
    ('GSM_GPRS_GCOMMON', 4004),
    ('GSM_GPRS_GLLC', 4005),
    ('GSM_GPRS_GMAC', 4006),
    ('GSM_GPRS_GPL1', 4007),
    ('GSM_GPRS_GRLC', 4008),
    ('GSM_GPRS_GRR', 4009),
    ('GSM_GPRS_GSNDCP', 4010),
    ('WLAN', 4500),
    ('WLAN_RESERVED_1', 4507),
    ('WLAN_RESERVED_2', 4508),
    ('WLAN_RESERVED_3', 4509),
    ('WLAN_RESERVED_4', 4510),
    ('WLAN_RESERVED_5', 4511),
    ('WLAN_RESERVED_6', 4512),
    ('WLAN_RESERVED_7', 4513),
    ('WLAN_RESERVED_8', 4514),
    ('WLAN_RESERVED_9', 4515),
    ('WLAN_RESERVED_10', 4516),
    ('WLAN_TL', 4517),
    ('WLAN_BAL', 4518),
    ('WLAN_SAL', 4519),
    ('WLAN_SSC', 4520),
    ('WLAN_HDD', 4521),
    ('WLAN_SME', 4522),
    ('WLAN_PE', 4523),
    ('WLAN_HAL', 4524),
    ('WLAN_SYS', 4525),
    ('WLAN_VOSS', 4526),
    ('ATS', 4600),
    ('MSGR', 4601),
    ('APPMGR', 4602),
    ('QTF', 4603),
    ('FWS', 4604),
    ('SRCH4', 4605),
    ('CMAPI', 4606),
    ('MMAL', 4607),
    ('MCS_RESERVED_5', 4608),
    ('MCS_RESERVED_6', 4609),
    ('MCS_RESERVED_7', 4610),
    ('MCS_RESERVED_8', 4611),
    ('IRATMAN', 4612),
    ('CXM', 4613),
    ('VSTMR', 4614),
    ('CFCM', 4615),
    ('DS', 5000),
    ('DS_SIO', 5013),
    ('DS_BCMCS', 5014),
    ('DS_MLRLP', 5015),
    ('DS_RTP', 5016),
    ('DS_SIPSTACK', 5017),
    ('DS_ROHC', 5018),
    ('DS_DOQOS', 5019),
    ('DS_IPC', 5020),
    ('DS_SHIM', 5021),
    ('DS_ACLPOLICY', 5022),
    ('DS_APPS', 5023),
    ('DS_MUX', 5024),
    ('DS_3GPP', 5025),
    ('DS_LTE', 5026),
    ('DS_WCDMA', 5027),
    ('DS_HDR', 5029),
    ('DS_IPA', 5030),
    ('DS_EPC', 5031),
    ('SEC', 5500),
    ('APPS', 6000),
    ('APPS_APPMGR', 6001),
    ('APPS_UI', 6002),
    ('APPS_QTV', 6003),
    ('APPS_QVP', 6004),
    ('APPS_QVP_STATISTICS', 6005),
    ('APPS_QVP_VENCODER', 6006),
    ('APPS_QVP_MODEM', 6007),
    ('APPS_QVP_UI', 6008),
    ('APPS_QVP_STACK', 6009),
    ('APPS_QVP_VDECODER', 6010),
    ('APPS_ACM', 6011),
    ('APPS_HEAP_PROFILE', 6012),
    ('APPS_QTV_GENERAL', 6013),
    ('APPS_QTV_DEBUG', 6014),
    ('APPS_QTV_STATISTICS', 6015),
    ('APPS_QTV_UI_TASK', 6016),
    ('APPS_QTV_MP4_PLAYER', 6017),
    ('APPS_QTV_AUDIO_TASK', 6018),
    ('APPS_QTV_VIDEO_TASK', 6019),
    ('APPS_QTV_STREAMING', 6020),
    ('APPS_QTV_MPEG4_TASK', 6021),
    ('APPS_QTV_FILE_OPS', 6022),
    ('APPS_QTV_RTP', 6023),
    ('APPS_QTV_RTCP', 6024),
    ('APPS_QTV_RTSP', 6025),
    ('APPS_QTV_SDP_PARSE', 6026),
    ('APPS_QTV_ATOM_PARSE', 6027),
    ('APPS_QTV_TEXT_TASK', 6028),
    ('APPS_QTV_DEC_DSP_IF', 6029),
    ('APPS_QTV_STREAM_RECORDING', 6030),
    ('APPS_QTV_CONFIGURATION', 6031),
    ('APPS_QCAMERA', 6032),
    ('APPS_QCAMCORDER', 6033),
    ('APPS_BREW', 6034),
    ('APPS_QDJ', 6035),
    ('APPS_QDTX', 6036),
    ('APPS_QTV_BCAST_FLO', 6037),
    ('APPS_MDP_GENERAL', 6038),
    ('APPS_GRAPHICS_GENERAL', 6040),
    ('APPS_GRAPHICS_EGL', 6041),
    ('APPS_GRAPHICS_OPENGL', 6042),
    ('APPS_GRAPHICS_DIRECT3D', 6043),
    ('APPS_GRAPHICS_SVG', 6044),
    ('APPS_GRAPHICS_OPENVG', 6045),
    ('APPS_GRAPHICS_2D', 6046),
    ('APPS_GRAPHICS_QXPROFILER', 6047),
    ('APPS_GRAPHICS_DSP', 6048),
    ('APPS_GRAPHICS_GRP', 6049),
    ('APPS_GRAPHICS_MDP', 6050),
    ('APPS_CAD', 6051),
    ('APPS_IMS_DPL', 6052),
    ('APPS_IMS_FW', 6053),
    ('APPS_IMS_SIP', 6054),
    ('APPS_IMS_REGMGR', 6055),
    ('APPS_IMS_RTP', 6056),
    ('APPS_IMS_SDP', 6057),
    ('APPS_IMS_VS', 6058),
    ('APPS_IMS_XDM', 6059),
    ('APPS_IMS_HOM', 6060),
    ('APPS_IMS_IM_ENABLER', 6061),
    ('APPS_IMS_IMS_CORE', 6062),
    ('APPS_IMS_FWAPI', 6063),
    ('APPS_IMS_SERVICES', 6064),
    ('APPS_IMS_POLICYMGR', 6065),
    ('APPS_IMS_PRESENCE', 6066),
    ('APPS_IMS_SIGCOMP', 6068),
    ('APPS_IMS_PSVT', 6069),
    ('APPS_IMS_UNKNOWN', 6070),
    ('APPS_IMS_SETTINGS', 6071),
    ('OMX_COMMON', 6072),
    ('APPS_IMS_RCS_CD', 6073),
    ('APPS_IMS_RCS_IM', 6074),
    ('APPS_IMS_RCS_FT', 6075),
    ('APPS_IMS_RCS_IS', 6076),
    ('APPS_IMS_RCS_AUTO_CONFIG', 6077),
    ('APPS_IMS_RCS_COMMON', 6078),
    ('APPS_IMS_UT', 6079),
    ('APPS_IMS_XML', 6080),
    ('ADSPTASKS', 6500),
    ('ADSPTASKS_KERNEL', 6501),
    ('ADSPTASKS_AFETASK', 6502),
    ('ADSPTASKS_VOICEPROCTASK', 6503),
    ('ADSPTASKS_VOCDECTASK', 6504),
    ('ADSPTASKS_VOCENCTASK', 6505),
    ('ADSPTASKS_VIDEOTASK', 6506),
    ('ADSPTASKS_VFETASK', 6507),
    ('ADSPTASKS_VIDEOENCTASK', 6508),
    ('ADSPTASKS_JPEGTASK', 6509),
    ('ADSPTASKS_AUDPPTASK', 6510),
    ('ADSPTASKS_AUDPLAY0TASK', 6511),
    ('ADSPTASKS_AUDPLAY1TASK', 6512),
    ('ADSPTASKS_AUDPLAY2TASK', 6513),
    ('ADSPTASKS_AUDPLAY3TASK', 6514),
    ('ADSPTASKS_AUDPLAY4TASK', 6515),
    ('ADSPTASKS_LPMTASK', 6516),
    ('ADSPTASKS_DIAGTASK', 6517),
    ('ADSPTASKS_AUDRECTASK', 6518),
    ('ADSPTASKS_AUDPREPROCTASK', 6519),
    ('ADSPTASKS_MODMATHTASK', 6520),
    ('ADSPTASKS_GRAPHICSTASK', 6521),
    ('L4LINUX_KERNEL', 7000),
    ('L4LINUX_KEYPAD', 7001),
    ('L4LINUX_APPS', 7002),
    ('L4LINUX_QDDAEMON', 7003),
    ('L4IGUANA_QDMS', 7102),
    ('L4IGUANA_REX', 7103),
    ('L4IGUANA_SMMS', 7104),
    ('L4IGUANA_KEYPAD', 7106),
    ('L4IGUANA_NAMING', 7107),
    ('L4IGUANA_SDIO', 7108),
    ('L4IGUANA_SERIAL', 7109),
    ('L4IGUANA_TIMER', 7110),
    ('L4IGUANA_TRAMP', 7111),
    ('L4AMSS_QDIAG', 7200),
    ('L4AMSS_APS', 7201),
    ('HIT', 8000),
    ('QDSP6', 8500),
    ('ADSP_RDA', 8524),
    ('UMB', 9000),
    ('LTE', 9500),
    ('LTE_MACDL', 9503),
    ('LTE_MACCTRL', 9504),
    ('LTE_RLCUL', 9505),
    ('LTE_RLCDL', 9506),
    ('LTE_PDCPUL', 9507),
    ('LTE_PDCPDL', 9508),
    ('LTE_ML1', 9509),
    ('QCHAT', 10200),
    ('QCHAT_CAPP', 10201),
    ('QCHAT_CENG', 10202),
    ('QCHAT_CREG', 10203),
    ('QCHAT_CMED', 10204),
    ('QCHAT_CAUTH', 10205),
    ('QCHAT_QBAL', 10206),
    ('QCHAT_OSAL', 10207),
    ('QCHAT_OEMCUST', 10208),
    ('QCHAT_MULTI_PROC', 10209),
    ('TDSCDMA_L2', 10252),
    ('TDSCDMA_MAC', 10253),
    ('TDSCDMA_RLC', 10254),
    ('TDSCDMA_RRC', 10255),
    ('CTA', 10300),
    ('QCNEA', 10350),
    ('QCNEA_CAC', 10351),
    ('QCNEA_CORE', 10352),
    ('QCNEA_CORE_CAS', 10353),
    ('QCNEA_CORE_CDE', 10354),
    ('QCNEA_CORE_COM', 10355),
    ('QCNEA_CORE_LEE', 10356),
    ('QCNEA_CORE_QMI', 10357),
    ('QCNEA_CORE_SRM', 10358),
    ('QCNEA_GENERIC', 10359),
    ('QCNEA_NETLINK', 10360),
    ('QCNEA_NIMS', 10361),
    ('QCNEA_NSRM', 10362),
    ('QCNEA_NSRM_CORE', 10363),
    ('QCNEA_NSRM_GATESM', 10364),
    ('QCNEA_NSRM_TRG', 10365),
    ('QCNEA_PLCY', 10366),
    ('QCNEA_PLCY_ANDSF', 10367),
    ('QCNEA_TEST', 10368),
    ('QCNEA_WQE', 10369),
    ('QCNEA_WQE_BQE', 10370),
    ('QCNEA_WQE_CQE', 10371),
    ('QCNEA_WQE_ICD', 10372),
    ('QCNEA_WQE_IFSEL', 10373),
    ('QCNEA_ATP', 10375),
    ('DPM', 10400),
    ('DPM_COMMON', 10401),
    ('DPM_COM', 10402),
    ('DPM_QMI', 10403),
    ('DPM_DSM', 10404),
    ('DPM_CONFIG', 10405),
    ('DPM_GENERIC', 10406),
    ('DPM_NETLINK', 10407),
    ('DPM_FD_MGR', 10408),
    ('DPM_CT_MGR', 10409),
    ('DPM_NSRM', 10410),
    ('DPM_NSRM_CORE', 10411),
    ('DPM_NSRM_GATESM', 10412),
    ('DPM_NSRM_TRG', 10413),
    ('DPM_TEST', 10414),
)

SUBSYSTEM_CODES = {}
SUBSYSTEM_NAMES = {}

for ss_name, ss_code in KNOWN_SUBSYSTEMS:
    SUBSYSTEM_CODES[ss_name] = ss_code
    SUBSYSTEM_NAMES[ss_code] = ss_name

set_rt_mask(0, 106, 0xffffffff)

#set_all_rt_masks(0xffffffff)

def decode_msg_header(data):
    (
        ts_type,
        num_args,
        drop_cnt,
        ts,
        line,
        ssid,
        ss_mask
    ) = struct.unpack('<BBBQ HHI', data[0:19])
    
    return ts_type, num_args, drop_cnt, ts, line, ssid, ss_mask, data[19:]

def decode_msg_args(num_args, data):
    args = []
    for i in range(num_args):
        args.append(struct.unpack('<I', data[:4]))
        data = data[4:]
    return args, data

def print_ext_msg_f(data):
    (
        ts_type,
        num_args,
        drop_cnt,
        ts,
        line,
        ssid,
        ss_mask,
        data
    ) = decode_msg_header(data)

    args, data = decode_msg_args(num_args, data)

    message, file = data.split(b'\x00')[:2]

    print('[{}] {}:{}: {}'.format(
        SUBSYSTEM_NAMES.get(ssid),
        file.decode('ascii'),
        line,
        message.decode('ascii')), args)

def print_qsr_ext_msg_terse_f(data):
    (
        ts_type,
        num_args,
        drop_cnt,
        ts,
        line,
        ssid,
        ss_mask,
        data
    ) = decode_msg_header(data)

    (msg_hash,) = struct.unpack('<I', data[:4])

    args, data = decode_msg_args(num_args, data[4:])

    print('[{}] {} aka {}'.format(
        SUBSYSTEM_NAMES.get(ssid),
        hex(msg_hash), msg_hash), args)

def unpack_multiple(packet):
    buf = None
    escape = 0
    for ch in packet:
        if buf is None:
            if ch == 0x7e:
                buf = b''
            else:
                raise ValueError('Invalid control char ' + hex(ch))
        elif ch == 0x7d:
            escape = 0x20
        elif ch == 0x7e:
            print(buf)
            yield buf[3], buf[4:]
            buf = None
        else:
            buf += struct.pack('<B', ch ^ escape)
            escape = 0

def process_cmd(cmd, cmddata):
    print(cmd)
    if cmd == DIAG_EXT_MSG_F:
        print_ext_msg_f(cmddata)
    elif cmd == DIAG_EXT_MSG_TERSE_F:
        print('DIAG_EXT_MSG_TERSE_F', cmddata)
    elif cmd == DIAG_QSR_EXT_MSG_TERSE_F:
        print_qsr_ext_msg_terse_f(cmddata)
    elif cmd == DIAG_CMD_EXT_F:
        process_cmd(cmddata[7], cmddata[8:])
    elif cmd == DIAG_QSR4_EXT_MSG_TERSE_F:
        print('DIAG_QSR4_EXT_MSG_TERSE_F', cmddata)

def recv_next():
    data = diag.sk.recv(BUF_SIZE)
    if data[0] == 0x7e:
        for packet in data[1:-1].split(b'~~'):
            process_cmd(packet[3], packet[4:])
    else:
        print(data)
        process_cmd(data[3], data[4:])

while True:
    try:
        recv_next()
    except KeyboardInterrupt:
        print()
        break
    except:
        continue
