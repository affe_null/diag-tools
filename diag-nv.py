# Copyright 2023 Affe Null
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in the COPYING.md file.
# If not, see <http://www.gnu.org/licenses/>.

import sys
import struct
from diag import Diag

diag = Diag()

DIAG_NV_READ_F = 38
DIAG_NV_WRITE_F = 39
DIAG_SUBSYS_CMD_F = 75

SUBSYS_NV = 48

def dump(data):
    recv = ""
    plain = ""
    if len(data) == 0:
        return ""
    for i in range(len(data)):
        inf = "%02X " % data[i]
        recv += inf
        if data[i] == 0x0D or data[i] == 0x0A or (0x20 <= data[i] <= 0x9A):
            plain += chr(data[i])
        else:
            plain += " "
        if ((i + 1) % 16) == 0:
            recv += "\n"
            plain += "\n"
    res = recv + "\n-----------------------------------------------\n"
    if len(plain.replace(" ", "").replace("\n", "")) > 0:
        res += plain
    print(res)

if len(sys.argv) == 2:
    item_num = int(sys.argv[1])
    data = struct.pack('<H128sH', item_num, b'\0' * 128, 0)
    dump(diag.send_cmd(DIAG_NV_READ_F, data=data)[3:])
elif len(sys.argv) == 3:
    item_num = int(sys.argv[1])
    index = int(sys.argv[2])
    data = struct.pack('<HH128sH', item_num, index, b'\0' * 128, 0)
    dump(diag.send_cmd(DIAG_SUBSYS_CMD_F, SUBSYS_NV, 1, 0, data=data)[8:])
