# Copyright 2023 Affe Null
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in the COPYING.md file.
# If not, see <http://www.gnu.org/licenses/>.

import sys
import binascii
from diag import Diag

diag = Diag()
DIAG_SPC_F = 65
print(diag.send_cmd(DIAG_SPC_F, data=binascii.unhexlify(sys.argv[1]))[1])
