# Copyright 2023 Affe Null
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in the COPYING.md file.
# If not, see <http://www.gnu.org/licenses/>.

import sys
import struct
from diag import Diag

diag = Diag()

DIAG_SUBSYS_CMD_F = 75

SUBSYS_FS = 19
SUBSYS_FS_ALT = 62

FS_HELLO = 0
FS_OPEN = 2
FS_CLOSE = 3
FS_READ = 4
FS_OPENDIR = 11
FS_READDIR = 12
FS_CLOSEDIR = 13

def fs_cmd(cmd, *args, path=None):
    data = b''
    for arg in args:
        data += struct.pack('<I', arg)
    if path is not None:
        data += path + b'\0'
    return diag.send_cmd(DIAG_SUBSYS_CMD_F, SUBSYS_FS, cmd, 0, data=data)

def handle_error(errno):
    if errno:
        raise OSError(errno)

fs_cmd(FS_HELLO, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

if sys.argv[1] == 'ls':
    [fd, errno] = struct.unpack('<II', fs_cmd(FS_OPENDIR, path=sys.argv[2].encode())[4:12])
    handle_error(errno)

    for seqno in range(1, 0xFFFFFFFF):
        resp = fs_cmd(FS_READDIR, fd, seqno)
        [fd, seqno, errno, entry_type, mode, size, atime, mtime, ctime] = struct.unpack('<IIIIIIIII',
                                                                                        resp[4:40])
        entry_name = resp[40:-1].decode()
        if entry_name == '':
            break
        print(f'{entry_type}\t{hex(mode)}\t{entry_name}')

    [errno] = struct.unpack('<I', fs_cmd(FS_CLOSEDIR, fd)[4:8])
    handle_error(errno)
elif sys.argv[1] == 'cat':
    [fd, errno] = struct.unpack('<II', fs_cmd(FS_OPEN, 0, 0, path=sys.argv[2].encode())[4:12])
    handle_error(errno)

    buf = b''
    bytes_read = -1
    while bytes_read != 0:
        resp = fs_cmd(FS_READ, fd, 512, len(buf))
        [fd, offset, bytes_read, errno] = struct.unpack('<IIII', resp[4:20])
        handle_error(errno)
        buf += resp[20:]

    with open('/dev/stdout', 'wb') as out:
        out.write(buf)

    [errno] = struct.unpack('<I', fs_cmd(FS_CLOSE, fd)[4:8])
    handle_error(errno)
