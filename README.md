# diag tools

These python scripts are meant to be run on a system with [diag-router](https://github.com/andersson/diag).

## Setting up diag-router on postmarketOS

To use diag-router on postmarketOS, install the `qcom-diag` and `rpmsgexport` packages using apk and copy the `50-rpmsg.rules` file from this repo into `/usr/lib/udev/rules.d`.

diag-router runs as root and can either be launched directly from the command line or enabled as a service using `rc-update add qcom-diag`.

**NOTE**: `diag-router` relies on a message sent by the modem at boot time. If you restart `diag-router`, you will also have to restart the modem (or reboot the device) to get these tools to work again.

## Usage

```
python3 diag-efs.py {cat|ls} {path}
python3 diag-log.py
python3 diag-nv.py {number}
python3 diag-nv.py {number} {sub-number}
python3 diag-spc.py 303030303030
```

## Useful information
- Use `diag-efs` to explore the modem's file system. For non-text files,
  use something like `python3 diag-efs.py cat /path/to/file | hd`
- The IMEI can be read with `python3 diag-nv.py 550`.
- Use the `diag-spc` command if diag-nv returns error 66.
