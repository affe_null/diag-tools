# Copyright 2023 Affe Null
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in the COPYING.md file.
# If not, see <http://www.gnu.org/licenses/>.
import struct
from socket import socket, AF_UNIX, SOCK_SEQPACKET

BUF_SIZE = 8192

class Diag:
    def __init__(self):
        self.sk = socket(AF_UNIX, SOCK_SEQPACKET, 0)
        self.sk.connect(b'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0')

    def send_cmd(self, *args, data=b''):
        hdr = b''
        for arg in args:
            hdr += struct.pack('<B', arg)
        self.sk.send(hdr + data)
        rsp = self.sk.recv(BUF_SIZE)
        if len(rsp) == 0:
            raise EOFError()
        if (len(args) == 0) or (rsp[0] == args[0]):
            return rsp
        else:
            print(rsp)
            raise RuntimeError('Command failed with status {}'.format(rsp[0]))

if __name__ == '__main__':
    print('This script cannot be used directly.')
